FROM centos

ADD myenv.sh /etc/profile.d/
ADD entrypoint.sh /

ENTRYPOINT ["/entrypoint.sh"]

